package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity;


import javax.persistence.*;

@Entity
@Table(name = "banking.transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String timeStamp;
    @Column
    private String typeTransaction;
    @Column
    private Long fromDeposit;
    @Column
    private Long toDeposit;
    @Column
    private Long userIdInit;
    @Column
    private double amount;
}
