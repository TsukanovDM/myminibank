package ru.sbrf.javalessons.tsukanovda.MyMiniBank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.services.UserServices;

@SpringBootApplication
@EnableJpaRepositories(enableDefaultTransactions = false)
public class MyMiniBankApplication {
    @Autowired
    private UserServices userService;

    public static void main(String[] args) {
        SpringApplication.run(MyMiniBankApplication.class, args);
    }
    }


