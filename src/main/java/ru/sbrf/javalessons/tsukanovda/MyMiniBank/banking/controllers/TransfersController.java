package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.services.UserServices;

import java.util.List;

@Controller

public class TransfersController {
    @Autowired
    UserServices userServices;


    @RequestMapping(value = "/transfers", method = RequestMethod.GET)
    public String getTransfersPage (Model model) {
        return "transfers";
    }

    @RequestMapping(value = "/transfers/between", method = RequestMethod.GET)
    public String getTransfersBetweenPage (Model model, User user)  {
        user = userServices.createUserSession();
        List<Long> cardsNumbers = userServices.getCardNumberByUserLogin(user.getLogin());
        model.addAttribute("cardsNumbers",cardsNumbers);
        return "between";
    }
}
