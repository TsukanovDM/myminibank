package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity.Card;

@Repository
public interface ContributionRepository extends JpaRepository<Card, Long> {
    @Query(value = "select contributions.balance from csa_login.users inner join banking.contributions on users.id = contributions.user_id  \n" +
            "where users.login = :login", nativeQuery = true)
    String getBalanceContributionByLogin(@Param("login") String login);


}
