package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity.Card;


@Repository
public interface CardRepository extends JpaRepository <Card,Long> {
   @Query(value = "select cards.balance from csa_login.users inner join banking.cards on users.id = cards.user_id  \n" +
           "where users.login = :login", nativeQuery = true)
   String getBalance(@Param("login") String login);

   @Query(value = "select cards.number from csa_login.users inner join banking.cards on users.id = cards.user_id  \n" +
           "where users.login = :login", nativeQuery = true)
   String getNumber(@Param("login") String login);

}
