package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity.Card;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity.Contribution;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.services.ContributionServices;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.services.UserServices;

@Controller
public class MainScreenController {
    @Autowired
    private UserServices userServices;
    @Autowired
    private ContributionServices contributionServices;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMainScreen(Model model, User user) {
        User curUser = userServices.createUserSession();
        model.addAttribute("balance_card", userServices.getCardNumberByUserLogin(user.getLogin()));
        model.addAttribute("balance_contribution", contributionServices.getBalance(curUser.getLogin()));
        return "index";

    }


}