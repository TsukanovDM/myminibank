package ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "INSERT INTO csa_login.users(" +
            "login,password,\"cardNumber\",authority,name,email)" +
            "VALUES (:login,:password,:cardNumber,'ROLE_USER',:name,:email)", nativeQuery = true)
    void InsertNewUser(@Param("login") String login,
                       @Param("password") String password,
                       @Param("cardNumber") Long cardNumber,
                       @Param("name") String name,
                       @Param("email") String email);

    @Query(value = "select cards.number from csa_login.users inner join banking.cards on users.id = cards.user_id  \n" +
            "where users.login = :login", nativeQuery = true)
    List<Long> getNumber(@Param("login") String login); //may be use hashMap for get number,balance

    @Query(value = "select cards.balance from csa_login.users inner join banking.cards on users.id = cards.user_id  \n" +
            "where users.login = :login", nativeQuery = true)
    String getBalance(@Param("login") String login);


}