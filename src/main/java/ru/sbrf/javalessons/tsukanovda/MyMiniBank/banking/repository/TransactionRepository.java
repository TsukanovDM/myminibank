package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.entity.Transaction;

public interface TransactionRepository extends JpaRepository <Transaction,Long> {
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    @Modifying(clearAutomatically = true)
    @Query(value = "update",nativeQuery = true)
    void getTransactBetween();
}
