package ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CreditsController {
    @RequestMapping(value = "/credits", method = RequestMethod.GET)
    public String getTransfersPage (Model model) {
        return "credits";
    }
}
