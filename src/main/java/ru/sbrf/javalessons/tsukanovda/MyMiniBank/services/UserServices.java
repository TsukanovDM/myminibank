package ru.sbrf.javalessons.tsukanovda.MyMiniBank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.repository.UserRepository;

import java.util.List;


@Service
public class UserServices {
    @Autowired
    private final UserRepository userRepository;

    public UserServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void InsertNewUser(String login, String password, Long cardNumber, String name, String email) {
        userRepository.InsertNewUser(login, password, cardNumber, name, email);
    }

    public String getBalanceByUserLogin (String login) {
        return userRepository.getBalance(login);
    }

    public List<Long> getCardNumberByUserLogin (String login) {
        return userRepository.getNumber(login);
    }

    public User createUserSession() {
        User user = new User();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        user.setLogin(authentication.getName());
        //user.setCardNumber(getCardNumberByUserLogin(user.getLogin()));
        return user;
    }
}

