package ru.sbrf.javalessons.tsukanovda.MyMiniBank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.banking.repository.ContributionRepository;

@Service
public class ContributionServices {
    @Autowired
    ContributionRepository contributionRepository;

    public ContributionServices(ContributionRepository contributionRepository) {this.contributionRepository = contributionRepository;}

    public String getBalance (String login) {
        return contributionRepository.getBalanceContributionByLogin(login);
    }
}
