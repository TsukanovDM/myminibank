package ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;

@Controller
public class CsaController {
    @GetMapping("/login")
    public String get(Model model, User user) {
        return "login";
    }

}
