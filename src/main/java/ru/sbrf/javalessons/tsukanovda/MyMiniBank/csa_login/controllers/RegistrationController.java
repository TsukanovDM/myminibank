package ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.csa_login.entity.User;
import ru.sbrf.javalessons.tsukanovda.MyMiniBank.services.UserServices;


@Controller
public class RegistrationController {
    @Autowired
    private UserServices userService;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(8);


    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String insertNewUser(Model model, User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.InsertNewUser(user.getLogin(),
                user.getPassword(),
                user.getCardNumber(),
                user.getName(),
                user.getEmail());
        return "redirect:/login";
    }
}
